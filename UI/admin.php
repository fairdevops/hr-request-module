<?php require('header.php'); ?>
	
<body>

    <nav class="navbar navbar-dark sticky-top flex-md-nowrap p-0" style="background-color: #0071ec;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Fairfirst</a>
      
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar" >
          <div class="sidebar-sticky">
            
              <img src="Images/logo.png" style="padding-left: 50px; padding-bottom: 20px; padding-top: 10px;">
            
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Recieved Requests <span class="sr-only">(current)</span>
                </a>
              </li>
             <li class="nav-item">
                <a class="nav-link" href="">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Approved Requests <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Rejected Requests
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Settings
                </a>
              </li>
              
            </ul>
          </div>
        </nav>
          <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
              <h4 class="h4">Recieved Requests</h4>
            </div>
            <form id="recieve_req_form" action="" method="post" style="padding-right: 20px; padding-left:20px; padding-top: 20px; padding-bottom: 20px; border: 1px solid;border-color: #0071ec; ">
            <div class="table-responsive">

              <table id="recieve_req" class="display nowrap dataTable dtr-inline collapsed hero-callout" >
                <thead>
                  <tr>
                    <th></th>
                    <th>#</th>
                    <th>EPF No.</th>
                    <th>Date Time</th>
                    <th>Type</th>
                    <th>Purpose</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                
                <tbody>

                  <?php 
                $con=mysqli_connect("localhost","root","","hr_req_letters");
                if($con-> connect_error)
                {
                  die("connection Failed".$con-> connect_error);
                }

                $q="SELECT * FROM request_details r, user u, letter_type lt where r.USER_ID=u.USER_ID and lt.L_ID=r.LET_TYPE";

                $result = $con -> query($q);
                $i=1;

               

                if($result -> num_rows >0)
                {
                  while($row= $result -> fetch_assoc()){
                            ?>

                   
                  <tr data-child-value=" ">
                    <td class="details-control"></td>
                    <?php
                   echo "<td>".$i."</td>";
                   echo "<td>".$row["EPF_NO"]."</td>";
                   echo "<td>".$row["REQ_DATE"]."</td>";
                   echo "<td>".$row["L_TYPE"]."</td>";
                   echo "<td>".$row["LET_PURPOSE"]."</td>";
                   echo "<td>".$row["LET_REMARKS"]."</td></tr>";
                   

                  $i++;
                  }

                  echo "</tbody>";
                }
                else{
                  echo "0 Results";
                }

                $con -> close();
               ?> 
              </table>
            </div>
          </form>
          </main>
         </div>
    </div>




 
    <script src="bootstrap-4.4.1-dist\js\feather.min.js"></script>
    <script>
      feather.replace()
    </script>
   
  </body>
  
<?php require('footer.php'); ?>
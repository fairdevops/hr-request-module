<?php require('header.php'); ?>
	
<body>
<nav class="navbar navbar-dark sticky-top flex-md-nowrap p-0" style="background-color: #0071ec;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Fairfirst</a>
      
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar" >
          <div class="sidebar-sticky">
            
              <img src="Images/logo.png" style="padding-left: 50px; padding-bottom: 20px; padding-top: 10px">
            
            <ul class="nav flex-column">
             <li class="nav-item">
                <a class="nav-link" href="user.php">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  New Request<span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="oldreq.php">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Old Requests <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pendingreq.php">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Pending Requests
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="approvedreq.php">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Approved Requests
                </a>
              </li>
              
            </ul>
          </div>
        </nav>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
 	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h4 class="h4">Approved Requests</h4>
    </div>
    <form action="" method="" style="padding-right: 20px; padding-left:20px; padding-top: 20px; padding-bottom: 20px; border: 1px solid;border-color: #0071ec; ">
    <div class="table-responsive">
        <table id="app_req" class="display nowrap dataTable dtr-inline collapsed" >
          <thead>
            <tr>
              <th>#</th>
              <th>Date</th>
              <th>Type</th>
              <th>Purpose</th>
              <th>Remarks</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
    </div>
    </form>
</main>

    

 
    <script src="bootstrap-4.4.1-dist\js\feather.min.js"></script>
    <script>
      feather.replace()
    </script>
</body>
	
<?php require('footer.php'); ?>
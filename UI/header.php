<!DOCTYPE html>
<html>
<head>
	<title>Request Letters</title>
  <script src="bootstrap-4.4.1-dist\js\jquery.js"></script>
  <script src='bootstrap-4.4.1-dist\js\jquery.min.js'></script>
  <script src="bootstrap-4.4.1-dist\js\jquery.validate.min.js"></script>
	<link rel="stylesheet" href="bootstrap-4.4.1-dist\css\bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="bootstrap-4.4.1-dist\js\bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="bootstrap-4.4.1-dist\js\jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="DataTables\DataTables-1.10.20\css\jquery.dataTables.min.css">
<script type="text/javascript" src="DataTables\DataTables-1.10.20\js\jquery.dataTables.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $("#req_purpose").change(function () {
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="2"){
                $(".box").not(".bnk_d").hide();
                $(".bnk_d").show();
                $(".stbox").hide();
            }
            else if($(this).attr("value")=="3"){
                $(".box").not(".visa_b").hide();
                $(".visa_b").show();
                $(".stbox").hide();
            }
            else if($(this).attr("value")=="4"){
                $(".box").not(".gov_dept").hide();
                $(".gov_dept").show();
                $(".stbox").hide();
            }
            else if($(this).attr("value")=="5"){
            
                  	$(".box").not(".study_b").hide();
                	$(".study_b").show();
                	$(".stbox").hide();
            }
            else if($(this).attr("value")=="6"){
                $(".box").not(".other_b").hide();
                $(".other_b").show();
                $(".stbox").hide();
            }
            else{
                $(".box").hide();
                $(".stbox").hide();
            }
        });
    }).change();
});

$(document).ready(function(){
    $("#st_p").change(function () {
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="p5"){
                $(".stbox").not(".st_other_b").hide();
                $(".st_other_b").show();
            }
            else
            {
            	$(".stbox").hide();
            }
        });
    }).change();
});

$(function ()
{
$("#req_form").validate({
    messages: {
     doc_type: {
      required: "*Please select a letter type."
     },
     req_purpose:{
      required: "*Please select the purpose of the request."
     },
     bk_name:{
      required: "*Please select a bank name from the given list."
     },
     brnch_name:{
      required: "*Please select a branch name from the given list."
     },
     vi:{
      required: "*Please select a embassy from the given list."
     },
     dpt:{
      required: "*Please select a department from the given list."
     },
     add:{
      required: "*Please select an address."
     },
     st_p:{
      required: "*Please select a study programme."
     },
     o_p:{
      required: "*Please enter your purpose of the request here."
     },
     o_studyprogramme:{
      required: "*Please enter your study programme here."
     }
    }
});
});




$(document).ready( function () {
    $('#old_req').DataTable();
} );

$(document).ready( function () {
    $('#pend_req').DataTable();
} );

$(document).ready( function () {
    $('#app_req').DataTable();
} );

$(document).ready( function () {
    $('#recieve_req').DataTable();
} );

function format (d) {
    return '<div>'+d+'</div>'+
    '<div align="right">'+
      '<input type="button" name="approve" id="approve" value="View Request" class="btn btn-outline-primary">'+
      '<canvas style="width:5px; height:10px;"></canvas>'+
      '<input type="button" name="reject" id="reject" value="Reject" class="btn btn-outline-danger">'+
    '</div>';

      
        
}

$(document).ready(function() {

    var table = $('#recieve_req').DataTable({

      "bDestroy":true
    });

    $('#recieve_req').on('click', 'td.details-control', function () {
          var tr = $(this).closest('tr');
          var row = table.row(tr);

          if (row.child.isShown()) {
              
              row.child.hide();
              tr.removeClass('shown');
          } else {
              
              row.child(format(tr.data('child-value'))).show();
              tr.addClass('shown');
          }
      });

  });

 

</script>

<style type="text/css">
	.sidebar {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  z-index: 100; /* Behind the navbar */
  padding: 48px 0 0; /* Height of navbar */
  box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);
}

.sidebar-sticky {
  position: relative;
  top: 0;
  height: calc(100vh - 48px);
  padding-top: .5rem;
  overflow-x: hidden;
  overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
}

.box{
        padding: 20px;
        display: none;
        border: 1px solid #0071ec;
        padding-top: 30px;
        margin-bottom: 10px;
        width: 1240px;
    }

    .bnk_d{}
    .visa_b{}
    .gov_dept{}
    .study_b{}
    .other_b{}
    .st_other_b{}

    .stbox{
        padding: 20px;
        display: none;
        border: 1px solid #0071ec;
        padding-top: 30px;
        margin-bottom: 10px;
        width: 1240px;
    }

td.details-control {
    background: url('Images/more.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('Images/close.png') no-repeat center center;

}

</style>
<style>

label.error {
  display: block;
  color: red;
  font-style: italic;
  font-weight: normal;
  font-size: 12px;
}
</style>

</head>
<?php require('header.php'); ?>
	
<body>

    <nav class="navbar navbar-dark sticky-top flex-md-nowrap p-0" style="background-color: #0071ec;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Fairfirst</a>
      
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar" >
          <div class="sidebar-sticky">
            
              <img src="Images/logo.png" style="padding-left: 50px; padding-bottom: 20px; padding-top: 10px;">
            
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="user.php">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  New Request <span class="sr-only"></span>
                </a>
              </li>
             <li class="nav-item">
                <a class="nav-link" href="oldreq.php">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Old Requests <span class="sr-only"></span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pendingreq.php">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Pending Requests
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="approved.php">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Approved Requests
                </a>
              </li>
              
            </ul>
          </div>
        </nav>
        
			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
				
          		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            		<h4 class="h4">Place a Request</h4>
            	</div>
             <form id="req_form" name="req_form" action="" method="post" style="padding-right: 20px; padding-left:20px; padding-top: 20px; padding-bottom: 20px; border: 1px solid;border-color: #0071ec; ">
            <div>
              
            	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            		    
                    <div style="padding-bottom: 14px;">
                    <label style="padding-right: 5px;"><h6>Select a Type</h6></label>
                    <select id="doc_type"  name="doc_type" style="width:450px;" class="required">
                      <option value="">...</option>
                      <option value="Employment Confirmation Letter">Employment confirmation letter</option>
                      <option value="Employment Confirmation with salary Letter">Employment confirmation with salary letter</option>
                    </select>
                    </div>
                    
              
                   
                   <div style="padding-bottom: 14px;">
                    <label style="padding-right: 5px;"><h6>Requesting for</h6></label>
                    <select id="req_purpose" name="req_purpose" style="width:450px;" class="required">
                      <option value="">...</option>
                      <option value="2">Bank/Credit Cards</option>
                      <option value="3">Visa</option>
                      <option value="4">Government Departments</option>
                      <option value="5">Study Programmes</option>
                      <option value="6">Other</option>
                    </select>
                    </div>
                </div>
            
            </div>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <div class="bnk_d box">
            <label style="padding-right: 5px;"><h6>Select the bank</h6></label>
            <select id="bk_name"  name="bk_name" style="width:350px;" class="required">
                      <option  value="">...</option>
                      <option value="Bank 1">Bank 1</option>
                      <option value="Bank 2">Bank 2</option>
                      <option value="Bank 3">Bank 3</option>
                      <option value="Bank 4">Bank 4</option>
                      <option value="Bank 5">Bank 5</option>
            </select>
            </div>
            <div class="bnk_d box">
            <label style="padding-right: 5px; padding-left: 5px;"><h6>Select the branch</h6></label>
            <select id="brnch_name" name="brnch_name" style="width:350px;" class="required">
                      <option value="">...</option>
                      <option value="Branch 1">Branch 1</option>
                      <option value="Branch 2">Branch 2</option>
                      <option value="Branch 3">Branch 3</option>
                      <option value="Branch 4">Branch 4</option>
                      <option value="Branch 5">Branch 5</option>
            </select>
            </div>
          

          <div class="visa_b box">
            <label style="padding-right: 5px;"><h6>Select the embassy</h6></label>
            <select id="vi"   name="vi" style="width:350px;" class="required">
                      <option  value="">...</option>
                      <option value="v1">Embassy 1</option>
                      <option value="v2">Embassy 2</option>
                      <option value="v3">Embassy 3</option>
                      <option value="v4">Embassy 4</option>
                      <option value="v5">Embassy 5</option>
            </select>
          </div>

          <div class="gov_dept box">
            <label style="padding-right: 5px;"><h6>Select the department</h6></label>
            <select id="dpt" name="dpt" style="width:350px;" class="required">
                      <option  value="">...</option>
                      <option value="d1">Department 1</option>
                      <option value="d2">Department 2</option>
                      <option value="d3">Department 3</option>
                      <option value="d4">Department 4</option>
                      <option value="d5">Department 5</option>
            </select>
            </div>
            <div class="gov_dept box">
            <label style="padding-right: 5px; padding-left: 5px;"><h6>Select the address</h6></label>
            <select id="add"  name="add" style="width:350px;" class="required">
                      <option value="">...</option>
                      <option value="a1">Address 1</option>
                      <option value="a2">Address 2</option>
                      <option value="a3">Address 3</option>
                      <option value="a4">Address 4</option>
                      <option value="a5">Address 5</option>
            </select>
          </div>

          <div class="study_b box">
            <label style="padding-right: 5px;"><h6>Select the study programme</h6></label>
            <select id="st_p"  name="st_p" style="width:350px;" class="required">
                      <option value="">...</option>
                      <option value="p1">Study Programme 1</option>
                      <option value="p2">Study Programme 2</option>
                      <option value="p3">Study Programme 3</option>
                      <option value="p4">Study Programme 4</option>
                      <option value="p5">Other</option>
            </select>

             
          </div>

          <div class="other_b box">
            <label style="padding-right: 5px;"><h6>Enter the purpose here</h6></label>
            <input type="text" id="o_p" name="o_p" style="width:500px;" class="required">
          </div>

        </div>
        <div class="st_other_b stbox">
          <label style="padding-right: 5px;"><h6>Enter the study programme here.</h6></label>
          <input type="text" id="o_studyprogramme" name="o_studyprogramme" style="width:500px;" class="required">
        </div>
            <div class="container-fluid">
            <h6>Remarks</h6>
              
                <textarea id="cmt"  name="cmt" rows="6" cols="171"></textarea>
              </div>
              <div class="container-fluid" align="right" style="padding-top: 20px;">

                <input type="submit" name="btn_sub" class="btn btn-m btn-outline-danger" value="Cancel">
                <input type="reset" class="btn btn-m btn-outline-warning" value="Reset">
          	     <input type="submit" class="btn btn-m btn-outline-success" value="Send Request">
              </div>

              <?php if(isset($_POST["btn_sub"]))
                {
                    $letter_type=$_POST["doc_type"];
                    $letter_purpose=$_POST["req_purpose"];
                    $bank_name=$_POST["bk_name"];
                    $branch_name=$_POST["brnch_name"];
                    $visa_emb=$_POST["vi"];
                    $gov_dept=$_POST["dpt"];
                    $dept_add=$_POST["add"];
                    $stdy_p=$_POST["st_p"];
                    $other_=$_POST["o_p"];
                    $ostdy_p=$_POST["o_studyprogramme"];
                    $rmks=$_POST["cmt"];


                    $con = mysqli_connect("localhost","root","","hr_req_letters");

                    if(!$con)
                      {
                        die("Cannot insert your details");
                      }
                    else
                      {
                        echo("You can insert your details");
                      }
      
                    $sql="";
    
    
    
                    if(mysqli_query($con,$sql))
                      {
                        echo("Data inserted.");
                       // header('location:ViewDetails.php');
                      }
                    else
                      {
                        echo("There is a problem in a query");
                      }
                } 
              ?>
                



          </form>
        </main>
       
       
      </div>
    </div>

   
    <script src="bootstrap-4.4.1-dist\js\feather.min.js"></script>
    <script>
      feather.replace()
    </script>
   
  </body>
	
<?php require('footer.php'); ?>

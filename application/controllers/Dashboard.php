<?php
class Dashboard extends CI_Controller {

        public function index(){

            $user=1;

            $this->load->model('request_model');
            $this->load->view('header.php');
            $data['old_request_data']= $this->request_model->get_all_requests($user);
            $data['total_p']= $this->request_model->total_pending_requests($user);
            $data['total_a']= $this->request_model->total_approved_requests($user);
            $data['total_r']= $this->request_model->total_rejected_requests($user);
            $this->load->view('user/user_dashboard',$data);
            $this->load->view('footer.php');

          
        }
       
}
<?php
class User extends CI_Controller {

       
        // public function view($page = 'home')
        // {
        // 	if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
	       //  {
	       //          // Whoops, we don't have a page for that!
	       //          show_404();
	       //  }

        // $data['title'] = ucfirst($page); // Capitalize the first letter

        // $this->load->view('header', $data);
        // $this->load->view('pages/'.$page, $data);
        // $this->load->view('user/footer', $data);
        // }


        public function index(){

                

                $this->load->model('letter_type_model');
                $this->load->model('bank_model');
                $this->load->model('institute_model');

                $this->load->model('request_model');

        	$this->load->view('header.php');
            

                $data['bank_data'] = $this->bank_model->get_bank_details();
                $data['institute_data'] = $this->institute_model->get_institute_details();
                $data['visa_embassy_data'] = $this->institute_model->get_visa_embassy_details();
                $data['study_programme_data'] = $this->institute_model->get_study_programme();
                $data['letter_type_data'] = $this->letter_type_model->get_letter_type_details();
                $data['request_data'] = $this->request_model->get_request_user(); 
                
                
                $this->load->view('user/new_request', $data);
        	$this->load->view('footer.php');
        }

        function get_users()
        {
                $user=$_POST['user_id'];
                $this->load->model('Request_user_model');
                $data['request_user_data'] = $this->Request_user_model->get_all_user_details($user);
                echo  json_encode($data);
        }

        function get_branch_byBid(){

                $bid = $_POST['bank_id'];
                $this->load->model('branch_model');
                $data['branch_data'] = $this->branch_model->get_branch_details($bid);
                
                
                echo  json_encode($data);
        }

        function get_dept_city()
        {
                $did = $_POST['institute_id'];
                $this->load->model('institute_model');
                $data['city_data'] = $this->institute_model->get_govdept_city($did);

                echo  json_encode($data);

        }

        function get_all(){
        	$this->load->model('Request_user_model');
        	
        	$data['user_data'] = $this->Request_user_model->get_all_user_details();

        	$this->load->view('user/doc_request',$data);
        }

        function place_request(){
                $this->load->model('request_model');

                if($this->input->post('btn_sub'))
                {
                $uid=1;
                $status='Pending';
                $bank_id=null;
                $br_id=null;
                $institute=null;
                $other_study=null;
                $other=null;
                $remarks=null;
                
                // $uid=$this->input->post('user_id_fk');

                $req_date=getdate(date("U"));
                $lastdate="$req_date[year]-$req_date[mon]-$req_date[mday] $req_date[hours]:$req_date[minutes]:$req_date[seconds]";
                $lettertype=$this->input->post('doc_type');
                $letter_purpose=$this->input->post('letter_purpose');


               if($letter_purpose=="Bank/Credit Cards")
                {
                        $bank_id=$this->input->post('bk_name');
                        $br_id=$this->input->post('brnch_name');
                }
                if($letter_purpose=="Study Programmes")
                {
                        $institute=$this->input->post('st_p');
                        if($institute=="p5")
                        {
                           $other_study=$this->input->post('o_studyprogramme');     
                        }
                }
                if($letter_purpose=="Government Departments")
                {
                        $institute=$this->input->post('dpt');
                }                
                if($letter_purpose=="Visa")
                {
                        $institute=$this->input->post('vi');
                }
                                        
                if($letter_purpose=="Other")
                {
                        $other=$this->input->post('o_p');
                } 
                $remarks=$this->input->post('cmt');
                
                
                $this->request_model->create_request($uid,$lastdate,$lettertype,$letter_purpose,$bank_id,$br_id,$other_study,$institute,$other,$remarks,$status);
                $this->load->view('header.php');  
                $this->load->view('user/request_placed');
                $this->load->view('footer.php');
                }
        }

        function request_status(){
                

                $this->load->model('request_model');

                if(($this->input->post('reject')))
                {
                        
                        $this->request_model->reject_request();
                }

                if(($this->input->post('approve')))
                {
                        
                        $this->request_model->approve_request();
                }
        }

      

        


}
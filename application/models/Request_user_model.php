<?php
class Request_user_model extends CI_Model {

       	public function __construct()
        {
        	$this->load->database();
        }

        public function get_all_user_details($user,$request){	
        	$this->db->select('*');
            $this->db->from('request_user_details');
            $this->db->join('request_details', 'request_user_details.user_id = request_details.user_id_fk');
            $this->db->where('user_id', $user);
            $this->db->where('request_id',$request);
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();        
        }

        public function get_user_purpose($user,$request){   
            $this->db->select('letter_purpose');
            $this->db->from('request_user_details');
            $this->db->join('request_details', 'request_user_details.user_id = request_details.user_id_fk');
            $this->db->where('user_id', $user);
            $this->db->where('request_id',$request);
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();        
        }

        public function get_user_details_for_letter_generation_institute($user,$request){   
            $this->db->select('*');
            $this->db->from('request_details');
            $this->db->join('request_user_details', 'request_user_details.user_id = request_details.user_id_fk');
            $this->db->join('institute_selection', 'institute_selection.institute_id = request_details.institute_id');
            $this->db->where('user_id', $user);
            $this->db->where('request_id',$request);
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();        
        }

        public function get_user_details_for_letter_generation_bank($user,$request){   
            $this->db->select('*');
            $this->db->from('request_details');
            $this->db->join('request_user_details', 'request_user_details.user_id = request_details.user_id_fk');
            $this->db->join('bank', 'bank.bank_id = request_details.bank_id_fk');
            $this->db->join('branch', 'branch.branch_id = request_details.branch_id_fk');
            $this->db->where('user_id', $user);
            $this->db->where('request_id',$request);
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();        
        }

        

       
}

?>
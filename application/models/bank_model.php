<?php
class bank_model extends CI_Model {

       	public function __construct()
        {
        	$this->load->database();
        }

        public function get_bank_details(){	
        	
            return $this->db->get('bank')->result();
        }

        public function create_bank($code,$name)
        {
        	$data = array('bank_code'=>$code,'bank_name'=>$name);
        	$str=$this->db->insert_string('bank',$data);
        }

        public function update_bank($id,$code,$name)
        {
        	$data = array('bank_code'=>$code,'bank_name'=>$name);
        	$where="bank_id=".$id;
        	$str=$this->db->update_string('bank',$data,$where);
        }

         public function delete_bank($id)
        {
        	$this->db->delete('bank', array('bank_id' => $id));
        }

       
}

?>
<?php
class branch_model extends CI_Model {

       	public function __construct()
        {
        	$this->load->database();
        }

        public function get_branch_details($bank){
            $this->db->select('branch_id,branch_name');
            $this->db->from('branch');	
            $this->db->where('bank_id_fk', $bank);
        	return $this->db->get()->result();
        }

        public function create_branch($bank_fk,$code,$name,$address1,$address2,$address3,$address4,$city,$contactno,$fax)
        {
        	$data = array(
                'bank_id_fk'=>$bank_fk,
                'branch_code'=>$code,
                'branch_name'=>$name,
                'address_line1'=>$addres1,
                'address_line2'=>$addres2,
                'address_line3'=>$addres3,
                'address_line4'=>$addres4,
                'city_fk'=>$city,
                'contact_no'=>$contactno,
                'fax'=>$fax);
        	$this->db->insert('branch',$data);
        }

        public function update_branch($bank_fk,$code,$name,$address1,$address2,$address3,$address4,$city,$contactno,$fax)
        {
        	$data = array(
                'bank_id_fk'=>$bank_fk,
                'branch_code'=>$code,
                'branch_name'=>$name,
                'address_line1'=>$addres1,
                'address_line2'=>$addres2,
                'address_line3'=>$addres3,
                'address_line4'=>$addres4,
                'city_fk'=>$city,
                'contact_no'=>$contactno,
                'fax'=>$fax);
        	$this->db->where("branch_id=".$id." and bank_id_fk=".$bank_fk." and city_fk=".$city);
        	$this->db->update('branch',$data,$where);
        }

         public function delete_branch($id)
        {
        	$this->db->delete('branch', array('branch_id' => $id));
        }

       
}

?>
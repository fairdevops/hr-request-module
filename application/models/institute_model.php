<?php
class institute_model extends CI_Model {

       	public function __construct()
        {
        	$this->load->database();
        }

        public function get_institute_details(){	
            $institute_type='government';
            $this->db->select('institute_id,institute_name');
            $this->db->where('institute_type', $institute_type);
        	return $this->db->get('institute_selection')->result();
        }

        public function get_visa_embassy_details(){ 
            $institute_type='visa';
            $this->db->select('institute_id,institute_name');
            $this->db->where('institute_type', $institute_type);
            return $this->db->get('institute_selection')->result();
        }

        public function get_study_programme(){ 
            $institute_type='study';
            $this->db->select('institute_id,institute_name');
            $this->db->where('institute_type', $institute_type);
            return $this->db->get('institute_selection')->result();
        }

        public function get_govdept_city($did){
            $this->db->select('city_list.city_name');
            $this->db->from('city_list');  
            $this->db->join('institute_selection', 'institute_selection.city_fk = city_list.city_id');
            $this->db->where('institute_selection.institute_id', $did);
            return $this->db->get()->result();
        }


        public function create_institute($name,$type,$address1,$address2,$address3,$address4,$city,$contactno,$fax)
        {
        	$data = array(
                'institute_name'=>$name,
                'institute_type'=>$type,
                'address_line1'=>$addres1,
                'address_line2'=>$addres2,
                'address_line3'=>$addres3,
                'address_line4'=>$addres4,
                'city_fk'=>$city,
                'contact_no'=>$contactno,
                'fax'=>$fax);
        	$this->db->insert('institute_selection',$data);
        }

        public function update_institute($id,$name,$type,$address1,$address2,$address3,$address4,$city,$contactno,$fax)
        {
        	$data = array(
                'institute_name'=>$name,
                'institute_type'=>$type,
                'address_line1'=>$addres1,
                'address_line2'=>$addres2,
                'address_line3'=>$addres3,
                'address_line4'=>$addres4,
                'city_fk'=>$city,
                'contact_no'=>$contactno,
                'fax'=>$fax);
        	$this->db->where("institute_id=".$id." and city_fk=".$city);
        	$this->db->update('institute_selection',$data);
        }

         public function delete_institute($id)
        {
        	$this->db->delete('institute_selection', array('institue_id' => $id));
        }

       
}

?>
<?php
class letter_type_model extends CI_Model {

       	public function __construct()
        {
        	// $this->load->database();
            parent::__construct();
        }

        public function get_letter_type_details(){	
        	return $this->db->get('letter')->result();
        }

        public function create_letter_type($name,$description)
        {
        	$data = array(
                'letter_type'=>$name,
                'letter_desc'=>$description
            );
        	$this->db->insert('letter',$data);
        }

        public function update_letter_type($id,$name,$description)
        {
        	$data = array(
                'letter_type'=>$name,
                'letter_desc'=>$description
            );
        	$this->db->where('letter_id='.$id);
        	$this->db->update('letter',$data);
        }

         public function delete_letter_type($id)
        {
        	$this->db->delete('letter', array('letter_id' => $id));
        }

       
}

?>
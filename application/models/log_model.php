<?php
class log_model extends CI_Model {

       	public function __construct()
        {
        	$this->load->database();
        }

        public function new_log($epf,$rid,$action,$logdate)
        {
        	$data = array(
                'log_datetime'=>$logdate,
                'request_id_fk'=>$rid,
                'action'=>$action,
                'epf_no'=>$epf
            );
        	$this->db->insert('request_log',$data);
        }
}

?>
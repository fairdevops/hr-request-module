<?php
class request_model extends CI_Model {

       	public function __construct()
        {
        	$this->load->database();
        }

        public function get_request_details(){	
        	return $this->db->get('request_details')->result();
        }

        public function get_request_user()
        {
            $this->db->select('*');
            $this->db->from('request_details');
            $this->db->join('request_user_details', 'request_user_details.user_id = request_details.user_id_fk');
            $this->db->join('letter', 'letter.letter_id = request_details.letter_type_fk');
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();
        }


        public function get_letter_type($letter)
        {
        	$this->db->select('letter_type');
            $this->db->from('letter');	
            $this->db->where('letter_id', $letter);
        	return $this->db->get()->result();
        }

        public function create_request($uid,$lastdate,$lettertype,$letter_purpose,$bank_id,$br_id,$other_study,$institute,$other,$remarks,$status)
        {
        	$data = array(
        			'user_id_fk' => $uid,
        			'last_updated_date' => $lastdate,
        			'letter_type_fk' => $lettertype,
        			'letter_purpose' =>$letter_purpose,
        			'bank_id_fk' => $bank_id,
        			'branch_id_fk' => $br_id,
        			'other_study_p' => $other_study,
        			'institute_id' => $institute,
        			'other_desc' => $other,
        			'letter_remarks' => $remarks,
        			'current_status' => $status
        			
					);

			$this->db->insert('request_details', $data);
        }

        public function reject_request($id)
        {
        	$status='Rejected';
        	$data=array('current_status' => $status);
            $this->db->where('request_id', $id);
        	$this->db->update('request_details', $data);

        }

        public function approve_request($id)
        {
        	$status='Approved';
        	$data=array('current_status' => $status);
            $this->db->where('request_id', $id);
        	$this->db->update('request_details', $data);

        }

        public function user_pending_requests($user){

            $status='Pending';
            $this->db->select('*');
            $this->db->from('request_user_details u, request_details r');
            $this->db->join('letter', 'letter.letter_id = r.letter_type_fk');
            $this->db->join('request_user_details ru','ru.user_id=r.user_id_fk');
            $this->db->where('ru.epf_no','00t971');
            $this->db->where('current_status',$status);
            $this->db->group_by("r.request_id");
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();
        }

        public function user_approved_requests($user){

            $status='Approved';
           $this->db->select('*');
            $this->db->from('request_user_details u, request_details r');
            $this->db->join('letter', 'letter.letter_id = r.letter_type_fk');
            $this->db->join('request_user_details ru','ru.user_id=r.user_id_fk');
            $this->db->where('ru.epf_no','00t971');
            $this->db->where('current_status',$status);
            $this->db->group_by("r.request_id");
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();
        }

        public function user_rejected_requests($user){

            $status='Rejected';
            $this->db->select('*');
            $this->db->from('request_user_details u, request_details r');
            $this->db->join('letter', 'letter.letter_id = r.letter_type_fk');
            $this->db->join('request_user_details ru','ru.user_id=r.user_id_fk');
            $this->db->where('ru.epf_no','00t971');
            $this->db->where('current_status',$status);
            $this->db->group_by("r.request_id");
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();
        }

        public function get_all_requests($user)
        {
            $this->db->select('*');
            $this->db->from('request_user_details u, request_details r');
            $this->db->join('letter', 'letter.letter_id = r.letter_type_fk');
            $this->db->join('request_user_details ru','ru.user_id=r.user_id_fk');
            $this->db->where('ru.epf_no','00t971');
            $this->db->group_by("r.request_id");
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();
        }

        public function total_pending_requests($user)
        {
            $status="Pending";
            $epf="00t971";
            // $this->db->where('user_id_fk', $user);
            $this->db->where('ru.epf_no', $epf);
            $this->db->where('r.current_status',$status);
            $this->db->group_by('ru.epf_no,r.request_id');
            return  $this->db->count_all_results('request_details r, request_user_details ru');


        }

        public function total_approved_requests($user)
        {
            $status="Approved";

            $this->db->where('current_status',$status);
            $this->db->where('user_id_fk', $user);
            return $this->db->count_all_results('request_details');
        }

        public function total_rejected_requests($user)
        {
            $status="Rejected";

            $this->db->where('current_status',$status);
            $this->db->where('user_id_fk', $user);
            return $this->db->count_all_results('request_details');
        }

        public function get_request_admin($status)
        {
            $this->db->select('*');
            $this->db->from('request_details');
            $this->db->join('request_user_details', 'request_user_details.user_id = request_details.user_id_fk');
            $this->db->join('letter', 'letter.letter_id = request_details.letter_type_fk');
            $this->db->where('current_status',$status);
            $this->db->order_by("last_updated_date", "desc");
            return $this->db->get()->result();
        }

       
}

?>
<body>
 

    <nav class="navbar navbar-dark sticky-top flex-md-nowrap p-0" style="background-color: #0071ec;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Fairfirst</a>
      
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar" >
          <div class="sidebar-sticky">
            
              <img src="<?=base_url('theme/assets/Images/logo.png');?>" style="padding-left: 50px; padding-bottom: 20px; padding-top: 10px;">
            
          </div>
        </nav>
          <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
              <h4 class="h4">Generate the letter</h4>
            </div>
            <div id="letter" name="letter">
            <?php
                        foreach($request_data as $row) 
                        {        
                          echo "</br>Department : ".$row->emp_dept." ";
                          echo "</br>Requesting for : ".$row->letter_purpose." "; 
                        }     

            	$doc_body="
              {Employer Name}</br>
              {Employer Position Title}</br>
              {Employer Department}</br>
              </br>
              {Date}</br>
              </br>
              RE: Employment Verification</br>
              </br>
              Dear {Name},</br>
              </br>
              I am sending this letter to confirm that has been employed at {Company} since {date}. {His/her} position title is {Title}.

              {Employee Name} works {full time/part time} for a total number of {number} days per week. {His/her} salary is {monetary amount} per {hour/month/year}.
              </br>
              Please feel free to contact me if you have any questions.</br>
              </br>
              Sincerely,</br>
              </br>
              {Employer Name}

              ";
					

			?>
        </div>

					<form name="export_form" action="<?php echo ($_SERVER['PHP_SELF']);?>" method="post">
    			<div align="right">	<input type="submit" name="submit_docs" value="Download" class="input-button btn btn-m btn-outline-success" />
            <input type="button" name="back" value="Go Back" class="input-button btn btn-m btn-outline-primary" onclick="window.location.href='Admin'" /> </div>
    				</form>

    				<?php
						if (isset($_POST['submit_docs'])) {
    						header("Content-Type:application/vnd.msword");
    						header("Expires: 0");
    						header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
       					header("content-disposition: attachment;filename=Letter.docx");
    						ob_clean();
                flush();
						}
							echo "<html>";
						  echo "$doc_body";
							echo "</html>";
					?>
          </main>
         </div>
    </div>




 
    <script src="<?=base_url('theme/assets/bootstrap-4.4.1-dist\js\feather.min.js');?>"></script>
    <script>
      feather.replace()
    </script>
   
  </body>

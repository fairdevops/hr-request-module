<body>

    <nav class="navbar navbar-dark sticky-top flex-md-nowrap p-0" style="background-color: #0071ec;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Fairfirst</a>
      
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar" >
          <div class="sidebar-sticky">
            
              <img src="<?=base_url('theme/assets/Images/logo.png');?>" style="padding-left: 50px; padding-bottom: 20px; padding-top: 10px;">
            
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="Admin">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Recieved Requests <span class="sr-only"></span>
                </a>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="Pending_request_admin_con">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Pending Requests <span class="sr-only"></span>
                </a>
              </li>
             <li class="nav-item">
                <a class="nav-link" href="Approve_request_admin_con">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Approved Requests <span class="sr-only"></span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Reject_request_admin_con">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Rejected Requests
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Settings_admin">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Settings
                </a>
              </li>
              
            </ul>
          </div>
        </nav>
          <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
              <h4 class="h4">Settings</h4>
            </div>
            <form id="req_form" name="req_form" action="" method="post" style="padding-right: 20px; padding-left:20px; padding-top: 20px; padding-bottom: 20px; ">
              <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            	<div style="padding-bottom: 14px;">
                    <label style="padding-right: 5px;"><h6>Select a Type</h6></label>
                    <select id="pid"  name="pid" style="width:450px;" class="required">
                      <option value="">--Please Select--</option>
                      <option value="Bank/Credit Cards">Bank/Credit Cards</option>
                      <option value="Visa">Visa</option>
                      <option value="Government Departments">Government Departments</option>
                      <option value="Study Programmes">Study Programmes</option>
                    </select>
                    </div>
                  </div>

          </main>
         </div>
    </div>




 
    <script src="<?=base_url('theme/assets/bootstrap-4.4.1-dist\js\feather.min.js');?>"></script>
    <script>
      feather.replace()
    </script>
   
  </body>
<footer>
<script src="<?=base_url('theme/assets/bootstrap-4.4.1-dist/js/jquery.validate.js');?>"></script>
<script type="text/javascript" src="<?=base_url('theme/assets/DataTables/DataTables-1.10.20/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript">


$(document).ready(function(){
    $("#letter_purpose").change(function () {
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="Bank/Credit Cards"){
                $(".box").not(".bnk_d").hide();
                $(".bnk_d").show();
                $(".stbox").hide();
            }
            else if($(this).attr("value")=="Visa"){
                $(".box").not(".visa_b").hide();
                $(".visa_b").show();
                $(".stbox").hide();
            }
            else if($(this).attr("value")=="Government Departments"){
                $(".box").not(".gov_dept").hide();
                $(".gov_dept").show();
                $(".stbox").hide();
            }
            else if($(this).attr("value")=="Study Programmes"){
            
                  	$(".box").not(".study_b").hide();
                	$(".study_b").show();
                	$(".stbox").hide();
            }
            else if($(this).attr("value")=="Other"){
                $(".box").not(".other_b").hide();
                $(".other_b").show();
                $(".stbox").hide();
            }
            else{
                $(".box").hide();
                $(".stbox").hide();
            }
        });
    }).change();
});

$(document).ready(function(){
    $("#st_p").change(function () {
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="p5"){
                $(".stbox").not(".st_other_b").hide();
                $(".st_other_b").show();
            }
            else
            {
            	$(".stbox").hide();
            }
        });
    }).change();
});

$(function ()
{
$("#req_form").validate({
    messages: {
     doc_type: {
      required: "*Please select a letter type."
     },
     req_purpose:{
      required: "*Please select the purpose of the request."
     },
     bk_name:{
      required: "*Please select a bank name from the given list."
     },
     brnch_name:{
      required: "*Please select a branch name from the given list."
     },
     vi:{
      required: "*Please select a embassy from the given list."
     },
     dpt:{
      required: "*Please select a department from the given list."
     },
     add:{
      required: "*Please select an address."
     },
     st_p:{
      required: "*Please select a study programme."
     },
     o_p:{
      required: "*Please enter your purpose of the request here."
     },
     o_studyprogramme:{
      required: "*Please enter your study programme here."
     }
    }
});
});




$(document).ready( function () {
    $('#old_req').DataTable();
} );

$(document).ready( function () {
    $('#pend_req').DataTable();
} );

$(document).ready( function () {
    $('#app_req').DataTable();
} );

$(document).ready( function () {
    $('#recieve_req').DataTable();
} );

function format ( d ) {
    return '<form method="post" action="<?=base_url('index.php/admin/request_status');?>" onsubmit="setTimeout(function(){window.location.reload();},10)"><div><table id="more_details" name="more_details" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"></table></div>'+
    '<div align="right">'+
      '<input type="submit" name="approve" id="approve" value="Approve" class="btn btn-outline-success" >'+
      '<canvas style="width:5px; height:10px;"></canvas>'+
      '<input type="submit" name="reject" id="reject" value="Reject" class="btn btn-outline-danger">'+
    '</div></from>';
}



$(document).ready(function() {


    var table = $('#recieve_req').DataTable({

      "bDestroy":true
    });
     
    
    $('#recieve_req').on('click', 'td.details-control', function (rowData) {

      console.log($(this).data('uid'));
      console.log($(this).data('rid'));

      var uid=$(this).data('uid');
      var rid=$(this).data('rid');

      $.ajax({
            type: "POST",
            data: {
                   user_id:uid,
                   request_id:rid
                  },
            url: "admin/get_users",
            dataType: "json",
            success: function(data) {
            var detailsHTML = "";
            
            for (var key in data["request_user_data"]) {
            
            detailsHTML += "<tr>";
            detailsHTML += "<tr><td>Request ID : " + data.request_user_data[key].request_id + "</td></tr>";
            detailsHTML += "<td>Name : " + data.request_user_data[key].emp_firstname +" "+data.request_user_data[key].emp_surname+ "</td></tr>";
            detailsHTML += "<tr><td>Department : " + data.request_user_data[key].emp_dept + "</td></tr>";
            detailsHTML += "<tr><td>Email : " + data.request_user_data[key].emp_email + "</td>";
            detailsHTML += "<tr><td>Status : " + data.request_user_data[key].current_status + "<canvas style='width:5px; height:10px;'></canvas>";
            if(data.request_user_data[key].current_status=="Approved")
            {
              detailsHTML+='<form id="" name="" action="<?= base_url('index.php/Generate_letter');?>"><input type="submit" name="gen_btn" id="gen_btn" value="Generate letter" class="btn btn-primary btn-sm">';
              detailsHTML += "<input type='hidden' id='uid' name='uid' value='"+data.request_user_data[key].user_id+"'>";
              detailsHTML += "<input type='hidden' id='rid' name='rid' value='"+data.request_user_data[key].request_id+"'>";
              detailsHTML += "<input type='hidden' id='reqp' name='reqp' value='"+data.request_user_data[key].letter_purpose+"'>";
              detailsHTML += "</form>";
            }
            detailsHTML += "</td></tr>";
            detailsHTML += "<input type='hidden' id='reqid' name='reqid' value='"+data.request_user_data[key].request_id+"'>";

        
            }
            

      
      $("#more_details").html(detailsHTML);
    }
  });

        var tr = $(this).closest('tr');
          var row = table.row(tr);

          if (row.child.isShown()) {
              
              row.child.hide();
              tr.removeClass('shown');
          } else {
              
              row.child(format(tr.data('child-value'))).show();
              tr.addClass('shown');
          }
    } );
} );

 $(document).ready(function(){
                        $("#bk_name").change(function() {
                          var b_id = $("#bk_name").val();
                        $.ajax({
                            type: "POST",
                            url: "<?=site_url('User/get_branch_byBid')?>",
                            dataType:'json',
                            data:
                            {
                              bank_id:b_id
                            },
                             success: function(data)
                             {
                              if (data.length == 0) 

                                document.getElementById("brnch_name").innerHTML = "<option>No branches</option>";
                              else
                              {
                                var optHTML = "<option value=''>--Please Select--</option>";

                                for (var key in data["branch_data"]) 
                                {
                                  optHTML += "<option value='"+data.branch_data[key].branch_id+"'>"  + data.branch_data[key].branch_name + "</option>";
                                }
                               document.getElementById("brnch_name").innerHTML = optHTML;
                              }
                            }
                          });
                        });
  });

 $(document).ready(function(){
                        $("#dpt").change(function() {
                          var gdept_id = $("#dpt").val();
                        $.ajax({
                            type: "POST",
                            url: "User/get_dept_city",
                            dataType:'json',
                            data:
                            {
                              institute_id:gdept_id
                            },
                             success: function(data)
                             {
                              if (data.length == 0) 

                                document.getElementById("add").innerHTML = "<option>...</option>";
                              else
                              {
                                var optHTML = "";
                                for (var key in data["city_data"]) 
                                {
                                  optHTML += "<option>"  + data.city_data[key].city_name + "</option>";
                                }
                               document.getElementById("add").innerHTML = optHTML;
                              }
                            }
                          });
                        });
  });


</script>

</footer>
</html>
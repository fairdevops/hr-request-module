	
<body>

    <nav class="navbar navbar-dark sticky-top flex-md-nowrap p-0" style="background-color: #0071ec;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Fairfirst</a>
      
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar" >
          <div class="sidebar-sticky">
            
              <img class="container-fluid" src="<?=base_url('theme/assets/Images/logo.png');?>" style="padding-left: 50px; padding-bottom: 20px; padding-top: 10px;">
            
            <?php include('user_navigation.php');?>
          </div>
        </nav>
        
			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
				
          		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            		<h4 class="h4">Place a Request</h4>
            	</div>
             <form id="req_form" name="req_form" action="<?= base_url('index.php/User/place_request');?>" method="post" style="padding-right: 20px; padding-left:20px; padding-top: 20px; padding-bottom: 20px; border: 1px solid;border-color: #0071ec; ">
            <div>
              
            	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            		    
                    <div style="padding-bottom: 14px;">
                    <label style="padding-right: 5px;"><h6>Select a Type</h6></label>
                    <select id="doc_type"  name="doc_type" style="width:450px;" class="required container-fluid">
                      <option value="">--Please Select--</option>
                      <?php foreach($letter_type_data as $row) 
                      { 
                        echo '<option value="'.$row->letter_id.'">'.$row->letter_type.'</option>';
                      }?> 
                    </select>
                    </div>
                    
              
                   
                   <div style="padding-bottom: 14px;">
                    <label style="padding-right: 5px;"><h6>Requesting for</h6></label>
                    <select id="letter_purpose" name="letter_purpose" style="width:450px;" class="required container-fluid">
                      <option value="">--Please Select--</option>
                      <option value="Bank/Credit Cards">Bank/Credit Cards</option>
                      <option value="Visa">Visa</option>
                      <option value="Government Departments">Government Departments</option>
                      <option value="Study Programmes">Study Programmes</option>
                      <option value="Other">Other</option>
                    </select>
                    </div>
                </div>
            
            </div>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <div class="bnk_d box">
            <label style="padding-right: 5px;"><h6>Select the bank</h6></label>
            <select id="bk_name"  name="bk_name" style="width:350px;" class="required container-fluid">
                      <option  value="">--Please Select--</option>
                     <?php foreach($bank_data as $row) 
                      { 
                        echo '<option value="'.$row->bank_id.'">'.$row->bank_name.'</option>';
                      }?>
                      
            </select>
            </div>
            <div class="bnk_d box">
            <label style="padding-right: 5px; padding-left: 5px;"><h6>Select the branch</h6></label>
            <script type="text/javascript">
            </script>
            <select id="brnch_name" name="brnch_name" style="width:350px;" class="required container-fluid">
                      <option value="">--Please Select--</option>
                      
            </select>
            </div>
          

          <div class="visa_b box">
            <label style="padding-right: 5px;"><h6>Select the embassy</h6></label>
            <select id="vi"   name="vi" style="width:350px;" class="required container-fluid">
                      <option  value="">--Please Select--</option>
                      <?php foreach($visa_embassy_data as $row) 
                      { 
                        echo '<option value="'.$row->institute_id.'">'.$row->institute_name.'</option>';
                      }?>
            </select>
          </div>

          <div class="gov_dept box">
            <label style="padding-right: 5px;"><h6>Select the department</h6></label>
            <select id="dpt" name="dpt" style="width:350px;" class="required container-fluid">
                      <option  value="">--Please Select--</option>
                      <?php foreach($institute_data as $row) 
                      { 
                        echo '<option value="'.$row->institute_id.'">'.$row->institute_name.'</option>';
                      }?>
            </select>
            </div>
            <div class="gov_dept box">
            <label style="padding-right: 5px; padding-left: 5px;"><h6>Select the address</h6></label>
            <select id="add"  name="add" style="width:350px;" class="required container-fluid">
                      <option value="">--Please Select--</option>
            </select>
          </div>

          <div class="study_b box">
            <label style="padding-right: 5px;"><h6>Select the study programme</h6></label>
            <select id="st_p"  name="st_p" style="width:350px;" class="required container-fluid">
                      <option value="">--Please Select--</option>
                     <?php foreach($study_programme_data as $row) 
                      { 
                        echo '<option value="'.$row->institute_id.'">'.$row->institute_name.'</option>';
                      }?>
                      <option value="p5">Other</option>
            </select>

             
          </div>

          <div class="other_b box">
            <label style="padding-right: 5px;"><h6>Enter the purpose here</h6></label>
            <input type="text" id="o_p" name="o_p" style="width:500px;" class="required container-fluid">
          </div>

        </div>
        <div class="st_other_b stbox">
          <label style="padding-right: 5px;"><h6>Enter the study programme here.</h6></label>
          <input type="text" id="o_studyprogramme" name="o_studyprogramme" style="width:500px;" class="required container-fluid">
        </div>
            <div class="container-fluid">
            <h6>Remarks</h6>
              
                <textarea id="cmt"  name="cmt" rows="6" cols="171" class="container-fluid"></textarea>
              </div>
              <div class="container-fluid" align="right" style="padding-top: 20px;">

                <input type="submit"  class="btn btn-m btn-outline-danger" value="Cancel">
                <input type="reset" class="btn btn-m btn-outline-warning" value="Reset">
          	     <input type="submit" name="btn_sub" class="btn btn-m btn-outline-success" value="Send Request">
              </div>

             

          </form>
        </main>
       
       
      </div>
    </div>

   
    <script src="<?=base_url('theme/assets/bootstrap-4.4.1-dist/js/feather.min.js');?>"></script>
    <script>
      feather.replace()
    </script>
   
  </body>

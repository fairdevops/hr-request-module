<body>
<nav class="navbar navbar-dark sticky-top flex-md-nowrap p-0" style="background-color: #0071ec;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Fairfirst</a>
      
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar" >
          <div class="sidebar-sticky">
            
              <img src="<?=base_url('theme/assets/Images/logo.png');?>" style="padding-left: 50px; padding-bottom: 20px; padding-top: 10px;">
            
            <?php include('user_navigation.php');?>
          </div>
        </nav>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

 	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h4 class="h4">Rejected Requests</h4>
    </div>
    <form action="" method="" style="padding-right: 20px; padding-left:20px; padding-top: 20px; padding-bottom: 20px; border: 1px solid;border-color: #0071ec; ">
    <div class="table-responsive">
        <table id="old_req" class="display nowrap dataTable dtr-inline collapsed" >
        	<thead>
            <tr>
              <th>#</th>
              <th>Date</th>
              <th>Type</th>
              <th>Purpose</th>
              <th>Remarks</th>
            </tr>
          </thead>
          <tbody>
                      <?php
                        $i=1;
                        foreach($rejected_request_data as $row) 
                        {
                          

                          ?>

                          <tr>
                      <?php
                            
                            echo "<td>".$i."</td>";
                            echo "<td>".$row->last_updated_date."</td>";
                            echo "<td>".$row->letter_type."</td>";
                            echo "<td>".$row->letter_purpose."</td>";
                            echo "<td>".$row->letter_remarks."</td></tr>";
                            $i++;
                        }
                      ?>
                </tbody>
        </table>
    </div>
    </form>
</main>

    

 
    <script src="<?=base_url('theme/assets/bootstrap-4.4.1-dist/js/feather.min.js');?>"></script>
    <script>
      feather.replace()
    </script>
</body>
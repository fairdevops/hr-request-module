<body>

    <nav class="navbar navbar-dark sticky-top flex-md-nowrap p-0" style="background-color: #0071ec;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Fairfirst</a>
      
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar" >
          <div class="sidebar-sticky">
            
              <img class="container-fluid" src="<?=base_url('theme/assets/Images/logo.png');?>" style="padding-left: 50px; padding-bottom: 20px; padding-top: 10px;">
            
            <?php include('user_navigation.php');?>
          </div>
        </nav>
        
			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
			    <div class="card-deck">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Pending Requests</h5>
                <p class="card-text">You have <?php echo $total_p?> pending requests in total.</p>
              </div>
            </div>
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Approved Requests</h5>
              <p class="card-text">You have <?php echo $total_a?> approved requests in total.</p>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Rejected Requests</h5>
              <p class="card-text">You have <?php echo $total_r?> rejected requests in total.</p>            
            </div>
          </div>
        </div>
<canvas style="height: 30px"></canvas>
    <div class="table-responsive">
        <table id="app_req" class="display nowrap dataTable dtr-inline collapsed" >
          <thead>
            <tr>
              <th>#</th>
              <th>EPF No.</th>
              <th>Date</th>
              <th>Type</th>
              <th>Purpose</th>
              <th>Remarks</th>
            </tr>
          </thead>
         <tbody>
                      <?php
                        $i=1;
                        foreach($old_request_data as $row) 
                        {
                          ?>
                          <tr>
                          
                      <?php
                            
                            echo "<td>".$i."</td>";
                            echo "<td>".$row->epf_no."</td>";
                            echo "<td>".$row->last_updated_date."</td>";
                            echo "<td>".$row->letter_type."</td>";
                            echo "<td>".$row->letter_purpose."</td>";
                            echo "<td>".$row->letter_remarks."</td></tr>";
                            $i++;
                        }
                      ?>
                </tbody>
        </table>
    </div>
        </main>
      </div>
    </div>

   
    <script src="<?=base_url('theme/assets/bootstrap-4.4.1-dist/js/feather.min.js');?>"></script>
    <script>
      feather.replace()
    </script>
   
  </body>
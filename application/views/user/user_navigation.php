<ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url('Dashboard')?>">
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  Dashboard 
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url()?>">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  New Request 
                </a>
              </li>
             <li class="nav-item">
                <a class="nav-link" href="<?=site_url('Approve_request_controller')?>">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Approved Requests 
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url('Pending_request_controller')?>">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Pending Requests
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url('Reject_request_controller')?>">
                  <span data-feather="file-text" style="color: #0071ec;"></span>
                  Rejected Requests
                </a>
              </li>
              
            </ul>